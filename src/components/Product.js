import React, {Component} from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    TouchableOpacity 
} from 'react-native';
import {connect} from 'react-redux';

import { addItemPedido, removeItemPedido } from '../store/actions/pedidoAction';

import { colors, metrics } from '../styles';

class Product extends Component {

    state = {
        amount: 0,
    };

    constructor(props){
       super(props);
    }

    _increment = () => {
        this.setState(prev => ({ amount: ++prev.amount }));
        const { name, price } = this.props;
        //let amount = this.state.amount + 1;
        let item = {};
        item[name] = this.state.amount + 1; 
        this.props.addItemPedido({ item, price});
    }

    _decrement = () =>{
        if (this.state.amount === 0) return null;
        const { name, price } = this.props;
        //let amount = this.state.amount - 1;
        let item = {};
        item[name] =  this.state.amount - 1;
        this.props.removeItemPedido({ item, price});
        this.setState(prev => ({ amount: --prev.amount }));
    }

    render(){
        return (
            <View style={ styles.box }>
                <View style={ styles.details } >
                    <Text style={ styles.name }>{this.props.name}</Text>
                    <Text style={ styles.price }>R$ {this.props.price}</Text>
                </View>
                
                <View style={ styles.quantity }>
                    <Text style={styles.quantityValue}>{this.state.amount}</Text>
                    <View>
                        <TouchableOpacity style={ styles.button } onPress={ () => this._increment() }>
                            <Text style={styles.textButtom}>+</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={ styles.button } onPress={ () => this._decrement() }>
                            <Text style={styles.textButtom}>-</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }


} 

const styles = StyleSheet.create({
    box:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.white,
        margin: metrics.baseMargin/2,
        paddingLeft: metrics.basePadding,
        borderRadius: metrics.baseRadius,
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowRadius: metrics.baseRadius,
        shadowOpacity: 0.2,
        elevation: 4, //Android only
    },
    name:{
        fontSize: 20,
        fontWeight: 'bold',
    },
    price:{
        fontSize: 15,
        marginTop: metrics.baseMargin,
    },
    quantity:{
        justifyContent: 'flex-end',
        flexDirection: 'row',
    },
    quantityValue:{
        fontSize: 30,
        alignSelf: 'center',
        paddingHorizontal: metrics.basePadding,
    },
    button:{
        paddingHorizontal: metrics.basePadding,
        paddingVertical: metrics.basePadding / 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textButtom:{
        fontSize: 25,
        color: colors.darkTransparent,
        fontWeight: 'bold',
    },
});


export default connect(null,
{
   addItemPedido,
   removeItemPedido,
})(Product);
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import { colors, metrics } from '../styles';
 
export default Header = props => (
    <View>
        <Text style={styles.title}>{props.nome}</Text>
    </View>
);

const styles = StyleSheet.create({
    title:{
        fontSize: 25,
        fontWeight: 'bold',
        color:  colors.white,
    }
});

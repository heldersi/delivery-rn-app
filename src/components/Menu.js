import React, {Component} from 'react';
import { View, FlatList } from 'react-native';

import { connect } from 'react-redux';
import firebase from 'firebase';

import Product from './Product';

import { changeProductList } from '../store/actions/productsActions';

class Menu extends Component {

    componentWillMount(){
        this._refresh();
    }

    _refresh = () =>{
        firebase.database().ref('products')
                .once('value')
                .then(snapshot =>{
                    this.props.changeProductList(snapshot.val());
                });
    }
    
    render(){
        return(
            <View>
                <FlatList
                    data={this.props.product_list}
                    renderItem={ ({item}) => <Product id={item.id} name={item.name} price={item.price} />}
                    keyExtractor={ (item) => item.id }
                    onRefresh={() => this._refresh()}
                    refreshing={false}
                />
            </View>
        )
    }

};

const mapStateToProps = state =>({
    product_list: state.productList,
});

export default connect(mapStateToProps, { changeProductList })(Menu);

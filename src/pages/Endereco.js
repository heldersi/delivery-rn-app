import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    StyleSheet,
    StatusBar,
    TextInput,
} from 'react-native';

import { colors, metrics } from '../styles';

import { connect } from 'react-redux';

import { 
  changeRua, 
  changeNumero, 
  changeBairro,
  changeNome,
} from '../store/actions/enderecoAction';
import { Actions } from '../../node_modules/react-native-router-flux';

import firebase from 'firebase';

class Endereco extends Component {

    _enviarPedido(){
      let endereco = this.props.endereco;
      let itens = this.props.itens;

      firebase.database().ref('pedidos')
        .push({ itens, endereco })
        .then( () => Actions.concluido() )
        .catch( err => null);
    }

    render(){
      return (
          <View style={ styles.container } >
              <StatusBar 
                  backgroundColor={ colors.darkPrimary}
                  barStyle="dark-content"
              />
              <TextInput
                style={ styles.input }
                onChangeText={ text => this.props.changeNome(text) }
                value={ this.props.endereco.nome }
                placeholder="Seu nome completo"
              />
              <TextInput
                style={ styles.input }
                onChangeText={ text => this.props.changeRua(text) }
                value={ this.props.endereco.rua }
                placeholder="Rua"
              />
              <TextInput
                style={ styles.input }
                onChangeText={ text => this.props.changeNumero(text) }
                value={ this.props.endereco.numero }
                placeholder="Número"
              />
              <TextInput
                style={ styles.input }
                onChangeText={ text => this.props.changeBairro(text) }
                value={ this.props.endereco.bairro }
                placeholder="Bairro"
              />
              <TouchableOpacity style={ styles.button } onPress={ () => this._enviarPedido() }>
                  <Text style={styles.textButtom}>Enviar Pedido</Text>
              </TouchableOpacity>
          </View>
      );
    }

}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
    },
    input:{
      height: 40, 
      borderColor: 'gray',
      margin: metrics.baseMargin,
      paddingHorizontal: metrics.basePadding,
      backgroundColor: colors.white
    },
    button:{
      height: 60,
      padding: metrics.basePadding,
      backgroundColor: colors.success,
      alignItems: 'center',
      justifyContent: 'center',
      margin: metrics.baseMargin,
      borderRadius: metrics.baseRadius,
    },
    textButtom:{
      fontSize: 20,
      color: colors.darkTransparent,
      fontWeight: 'bold',
    },
  });

const mapStateToProps = state =>({
    total: state.pedido.total,
    itens: state.pedido.itens,
    endereco: state.endereco,
})

export default connect(mapStateToProps, 
  { changeRua, changeNumero, changeBairro, changeNome }
)(Endereco);
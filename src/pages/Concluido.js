import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    StyleSheet,
} from 'react-native';

import { colors, metrics } from '../styles';

import {Actions} from 'react-native-router-flux';

export default class Concluido extends Component {

    render(){
      return (
          <View style={ styles.container } >
              <Text style={ styles.text } >Pedido registrado, a entrega será realizada em alguns minutos.</Text>
              <TouchableOpacity style={ styles.button } onPress={ () => Actions.popTo('pedido') }>
                  <Text style={styles.textButtom}>Página inicial</Text>
              </TouchableOpacity>
          </View>
      );
    }

}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
    },
    text:{
      alignSelf: 'center',
      fontSize: 20,
      margin: metrics.baseMargin,
    },
    button:{
      height: 60,
      padding: metrics.basePadding,
      backgroundColor: colors.success,
      alignItems: 'center',
      justifyContent: 'center',
      margin: metrics.baseMargin,
      borderRadius: metrics.baseRadius,
    },
    textButtom:{
      fontSize: 20,
      color: colors.darkTransparent,
      fontWeight: 'bold',
    },
  });
import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    StyleSheet,
    StatusBar,
} from 'react-native';

import Header from '../components/Header';
import Menu from '../components/Menu';

import { colors, metrics } from '../styles';

import { connect } from 'react-redux';

import { Actions } from 'react-native-router-flux';

class Pedido extends Component {

    _confirmarPedido(){
        Actions.endereco()
    }

    render(){
        //alert(JSON.stringify(this.props.itens));
        return (
            <View style={ styles.container } >
                <StatusBar 
                    backgroundColor={ colors.darkPrimary}
                    barStyle="light-content"
                />
                <View style={ styles.header }>
                   <Header nome='App Delivery' />
                </View>
                <View style={ styles.menu }>
                   <Menu />
                </View>    
                <View style={ styles.totalBox } >
                    <Text style={ styles.totalValue }> Total: R$ { this.props.total.toFixed(2).replace('.',',') } </Text>
                </View>  
                <TouchableOpacity style={ styles.button } onPress={ () => this._confirmarPedido() }>
                    <Text style={styles.textButtom}>Confirmar Pedido</Text>
                </TouchableOpacity>
                
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
    },
    header:{
      flex: 1,
      paddingTop: metrics.basePadding * 2,
      alignItems: 'center',
      backgroundColor: colors.primary,
    },
    menu:{
      flex: 8,
      padding: metrics.basePadding/2,
    },
    totalBox:{
      height: 40,
      alignItems: 'flex-end',
      paddingTop: 5,
      paddingHorizontal: metrics.basePadding,
    },
    totalValue:{
      fontSize: 25,
    },
    button:{
      flex: 0.5,
      padding: metrics.basePadding,
      backgroundColor: colors.success,
      alignItems: 'center',
      justifyContent: 'center',
      margin: metrics.baseMargin,
      borderRadius: metrics.baseRadius,
    },
    textButtom:{
      fontSize: 20,
      color: colors.darkTransparent,
      fontWeight: 'bold',
    },
  });

const mapStateToProps = state =>({
    total: state.pedido.total,
    itens: state.pedido.itens,
})

export default connect(mapStateToProps)(Pedido);
const INITIAL_STATE = {
    nome:"",
    rua: "",
    numero: "",
    bairro:"",
}

export default ( state = INITIAL_STATE, action) =>{
    switch(action.type){
        case 'CHANGE_RUA':
            return {...state, rua: action.payload};
        case 'CHANGE_NUMERO':
            return {...state, numero: action.payload};
        case 'CHANGE_BAIRRO':
            return {...state, bairro: action.payload};
        case 'CHANGE_NOME':
            return {...state, nome: action.payload};
        default:
            return state;
    }
}
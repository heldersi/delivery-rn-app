import { combineReducers } from 'redux';

import pedido from './pedido';
import productList from './productList';
import endereco from './endereco';

export default combineReducers({
    pedido,
    productList,
    endereco,
})

// global state
/*
{
    pedido: {
        itens:[],
        total: 0,
    },
    product_list:[],
    endereco: {
        rua: "",
        numero: "",
        bairro:"",
    }
}
*/
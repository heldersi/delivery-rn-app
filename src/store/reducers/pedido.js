const INITIAL_STATE = {
    itens: {},
    total: 0,
}

// objeto que representa uma action:
/*
{
    type: 'ACTION_TYPE',
    payload: info,
}
*/

// O reducer é uma função que retorna o state atualizado
// Recebe o state atual e a action disparada
// Todos os reducer "ouvem" todas as action disparadas
// De acordo com uma determinada action, uma alteração no state será aplicada
export default pedido = ( state = INITIAL_STATE, action ) =>{
    switch( action.type ){
        case 'ADD_ITEM':
            return {...state, 
                total: state.total + parseInt(action.payload.price),
                itens: {...state.itens, ...action.payload.item}
                };
        case 'REMOVE_ITEM':
            let itens = {...state.itens, ...action.payload.item};
            for( key in itens){
                if(itens[key] === 0) delete itens[key]
            }
            return {...state, 
                    total: state.total - parseInt(action.payload.price),
                    itens
                }
        default:
            return state;
    }
}
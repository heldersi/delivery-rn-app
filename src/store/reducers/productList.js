const INITIAL_STATE = [ 
];


// O reducer é uma função que retorna o state atualizado
// Recebe o state atual e a action disparada
// Todos os reducer "ouvem" todas as action disparadas
// De acordo com uma determinada action, uma alteração no state será aplicada
export default productList = ( state = INITIAL_STATE, action ) =>{
    switch(action.type){
        case 'CHANGE_PRODUCT_LIST':
            return action.payload;
        default:
            return state;
    }
}

export const changeRua = rua =>({
    type: 'CHANGE_RUA',
    payload: rua,
});

export const changeNumero = numero =>({
    type: 'CHANGE_NUMERO',
    payload: numero,
});

export const changeBairro = bairro =>({
    type: 'CHANGE_BAIRRO',
    payload: bairro,
});

export const changeNome = nome =>({
    type: 'CHANGE_NOME',
    payload: nome,
});
import { createStore } from 'redux';

import reducers from './reducers';

//Create store recebe: reducers(required), states, middlewares
const store = createStore( reducers );

export default store;
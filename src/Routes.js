import React from 'react';
import {Router, Scene} from 'react-native-router-flux';

import Pedido from './pages/Pedido';
import Endereco from './pages/Endereco';
import Concluido from './pages/Concluido';

export default props =>(
    <Router>
        <Scene key='root'>
            <Scene key='pedido' component={Pedido} hideNavBar/>
            <Scene key='endereco' component={Endereco} title='Endereço de entrega'/> 
            <Scene key='concluido' component={Concluido} title='Pedido recebido' hideNavBar /> 
        </Scene>
    </Router>
)